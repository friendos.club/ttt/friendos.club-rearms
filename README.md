# friendos.club map rearms for TTT

this repository holds rearm scripts for different TTT maps we have on the
[friendos.club TTT server][1].


## Currently Supported Maps

- `dm_winter_v2`
- `gm_construct`
- `gm_csgohalocrater`
- `gm_csgoinsertion`
- `gm_hotwireslum2016`
- `ttt_concrete_b3`
- `ttt_fezvillage`
- `ttt_glacier`
- `ttt_heaven`
- `ttt_island_2013`
- `ttt_lockout`
- `ttt_scarisland_b1`
- `ttt_schooldayv2`


## Planned Maps

- [ ] `cs_assault`
- [ ] `cs_compound`
- [ ] `cs_havana`
- [ ] `cs_italy`
- [ ] `cs_militia`
- [ ] `cs_office`
- [ ] `de_aztec`
- [ ] `de_cbble`
- [ ] `de_chateau`
- [ ] `de_dust`
- [ ] `de_dust2`
- [ ] `de_haunts`
- [ ] `de_inferno`
- [ ] `de_nuke`
- [ ] `de_piranesi`
- [ ] `de_port`
- [ ] `de_prodigy`
- [ ] `de_tides`
- [ ] `de_train`
- [ ] `de_wanda`
- [ ] `dm_winter_v2`
- [ ] `gm_construct`
- [ ] `gm_csgoagency`
- [ ] `gm_csgoapote`
- [ ] `gm_csgohalocrater`
- [ ] `gm_csgoinsertion`
- [ ] `gm_csgokowloon`
- [ ] `gm_csgotulip`
- [ ] `gm_flatgrass`
- [ ] `gm_hotwireslum2016`
- [ ] `gm_mttresort`
- [ ] `mu_hotwireslum2016`
- [ ] `ttt_abyss`
- [ ] `ttt_airbus_b3`
- [ ] `ttt_atlantis`
- [ ] `ttt_avalon`
- [ ] `ttt_backupnineteen`
- [ ] `ttt_bafford_manor_a5b`
- [ ] `ttt_bb_teenroom_b2`
- [ ] `ttt_bf3_ziba_tower`
- [ ] `ttt_black_mesa_east_v2`
- [ ] `ttt_camel_v1`
- [ ] `ttt_canvas_r246`
- [ ] `ttt_canyon_a4`
- [ ] `ttt_castillo`
- [ ] `ttt_castle_black_v5`
- [ ] `ttt_catacombs_v3`
- [ ] `ttt_cellblock-d_b2`
- [ ] `ttt_chaser_v2`
- [ ] `ttt_christmastown`
- [ ] `ttt_coastalcomplex2_r1`
- [ ] `ttt_concrete_b3`
- [ ] `ttt_crummycradle_a4`
- [ ] `ttt_crystalmaze`
- [ ] `ttt_csgobank`
- [ ] `ttt_cwoffice2022`
- [ ] `ttt_cyberia_a3`
- [ ] `ttt_deadwood_a2`
- [ ] `ttt_desertvilliage`
- [ ] `ttt_desperados`
- [ ] `ttt_diamondshoals_a2_d`
- [ ] `ttt_dolls`
- [ ] `ttt_donaldapplebee`
- [ ] `ttt_dp_cstore_v1`
- [ ] `ttt_dreamworld6_2018`
- [ ] `ttt_dwarfhole`
- [ ] `ttt_equilibre`
- [ ] `ttt_feudal_v1`
- [ ] `ttt_firelinkshrine`
- [ ] `ttt_frontier_land_b2`
- [ ] `ttt_goldrush`
- [ ] `ttt_gunkanjima_v2`
- [ ] `ttt_hairyhouse`
- [ ] `ttt_halfpipe_park`
- [ ] `ttt_ham`
- [ ] `ttt_happyhome`
- [ ] `ttt_heavenandhell`
- [ ] `ttt_highschool_v3`
- [ ] `ttt_hotwireslum2016`
- [ ] `ttt_hypa`
- [ ] `ttt_innocentmotel_v1`
- [ ] `ttt_interrogation`
- [ ] `ttt_island_2022`
- [ ] `ttt_lab`
- [ ] `ttt_labs`
- [ ] `ttt_lego_treasureisland`
- [ ] `ttt_lumbridge_a4`
- [ ] `ttt_lumos_v2`
- [ ] `ttt_metrotunnels`
- [ ] `ttt_mira_hq`
- [ ] `ttt_office_esp`
- [ ] `ttt_office_nightfire`
- [ ] `ttt_oilrig`
- [ ] `ttt_pluto_stacks`
- [ ] `ttt_powerplant`
- [ ] `ttt_ravenville_a2`
- [ ] `ttt_scarisland_withframes_a4`
- [ ] `ttt_shadowmoses`
- [ ] `ttt_shadowraid_v3`
- [ ] `ttt_strangetower_v1_1_1`
- [ ] `ttt_swimming_pool`
- [ ] `ttt_vegas`
- [ ] `ttt_waterfire2`
- [ ] `ttt_westwood_v4`
- [ ] `ttt_workinprogress`



[1]: https://www.gametracker.com/server_info/192.99.14.76:27315/
